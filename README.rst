===============
 setup_helpers
===============

This is a little helper script for ``setup.py`` files, containing a few
convenience functions.  Start by grabbing this file; it's not a package, isn't
available on PyPI... it's just a file!
::

    $ curl -O https://gitlab.com/warsaw/setup_helpers/raw/master/setup_helpers.py

Put this file next to your ``setup.py`` file and stick this at the top of your
``setup.py``::

    from setup_helpers import (
        description, get_version, long_description, require_python)


Minimum required Python version
===============================

You can assert the minimum required version of Python your package is
compatible with by adding the following, providing the hex version of the
minimum Python version.  E.g. for Python 3.5 or higher, use::

    require_python(0x30500f0)


Setting __version__
===================

It's useful to set your ``__version__`` string in ``setup.py`` and to feed
this value to the ``version`` key in the ``setup()`` call.  You'd also like
for people importing your project to have a programmatic ``__version__`` value
they can introspect.  Of course, you only want to set this once so it's easier
to update.  Now you can put the following in your package's ``__init__.py``
file::

    __version__ = '9.12b3'

and the following in your ``setup.py``::

    __version__ = get_version('mypackage/__init__.py')
    # ...
    setup(
        # ...
        version=__version__,
        # ...
        )

``get_version()`` is compatible with `PEP 440`_ style version strings.
``get_version()`` accepts an optional ``pattern`` argument for alternative
regular expressions.


Descriptions
============

You probably also want to set your ``setup()`` call's ``description`` and
``long_description`` keys.  Use the ``setup_helpers`` functions of the same
name to extract the text of these from existing README or other files.

``description()`` takes a single file name argument.  ``long_description()``
takes any number of file names and it concatenates the contents.  E.g.::

    setup(
        # ...
        description=description('README.rst'),
        long_description=long_description('README.rst', 'NEWS.rst')
        # ...


Author
======

``setup_helpers.py`` is Copyright (C) 2009-2017 Barry Warsaw <barry@python.org>

Licensed under the terms of the Apache License Version 2.0.  See the LICENSE
file for details.


Project
=======

* Project home: https://gitlab.com/warsaw/setup_helpers
* Report bugs at: https://gitlab.com/warsaw/setup_helpers/issues
* Code hosting: https://gitlab.com/warsaw/setup_helpers.git


.. _`PEP 440`: http://legacy.python.org/dev/peps/pep-0440/
